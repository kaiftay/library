package com.epam.library;

public class LibraryAplication {
    public static void main(String[] args) {
        Library libraryU = new UniversityLibrary(10);
        Library libraryS = new SchoolLibrary(10);
        for (int i = 0; i < 4; i++) {
            Book book = new Book("kniga " + i, "Petya " + i, i + 50, i + 1987, i % 2 == 0 ? "School" : "University");
            libraryS.addBook(book);
            libraryU.addBook(book);

        }
        System.out.println(libraryS);
        System.out.println(libraryU);
        Book book = libraryS.searchBook("kniga 2");
        libraryS.deleteBook(book);
        System.out.println(libraryS);
    }
}
