package com.epam.library;

import java.util.Arrays;

public class SchoolLibrary implements Library {
    private int index;

    private Book[] books;

    public SchoolLibrary(int size) {
        books = new Book[size];
    }

    @Override
    public void addBook(Book book) {
        if (index < books.length && isSupportedType(book))
            books[index++] = book;

    }

    @Override
    public void deleteBook(Book book) {
        int deleteIndex = 0;
        if (book == null)
            return;
        for (int i = 0; i < books.length; i++) {
            if (book.equals(books[i])) {
                deleteIndex = i;
                break;
            }
        }
        for (int i = deleteIndex; i < books.length-1; i++) {
            books[i] = books[i + 1];
        }
        books[index--]= null;
    }

    @Override
    public String toString() {
        return "SchoolLibrary{" +
                "books=" + Arrays.toString(books) +
                '}';
    }

    @Override
    public Book searchBook(String title) {
        for (int i = 0; i < index; i++) {
            if (title.equals(books[i].getTitle())) {
                return books[i];
            }
        }
        return null;
    }

    @Override
    public boolean isSupportedType(Book book) {
        return "School".equals(book.getType());
    }
}


