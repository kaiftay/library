package com.epam.library;

import java.util.Objects;

public class Book {
    private String title;
    private String author;
    private int pages;
    private int year;
    private int id;
    private static int count;
    private String type;

    public Book(String title, String author, int pages, int year, String type) {
        this.title = title;
        this.author = author;
        this.pages = pages;
        this.year = year;
        this.id = count++;
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public int getPages() {
        return pages;
    }

    public int getYear() {
        return year;
    }

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", pages=" + pages +
                ", year=" + year +
                ", id=" + id +
                ", type='" + type + '\'' +
                '}';
    }
}


