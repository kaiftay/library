package com.epam.library;

public interface Library {
    void addBook(Book book);

    void deleteBook(Book book);

    Book searchBook(String title);

    boolean isSupportedType(Book book);
}
